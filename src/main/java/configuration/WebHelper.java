package configuration;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class WebHelper {

    protected WebDriver driver;

    protected WebDriverWait wait;

    public WebHelper(WebDriver driver, WebDriverWait wait){
        this.driver = driver;
        this.wait = wait;
    }

    public void clickElement(By webElement){
        wait.until(ExpectedConditions.presenceOfElementLocated(webElement)).click();
    }

    public boolean isDisplayedElement(By webElement){
        try{
            WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(webElement));
            return element.isDisplayed();
        } catch(TimeoutException e){
            return false;
        }
    }

    public void waitForElementToBeInvisible(By webElement ){
        wait.until(ExpectedConditions.invisibilityOfElementLocated(webElement));
    }
}