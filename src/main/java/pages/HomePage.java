package pages;

import configuration.WebHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class HomePage extends WebHelper {

    public HomePage(WebDriver driver, WebDriverWait wait){
        super(driver, wait);
    }

    By searchInputField = By.id("search-box-input");
    By searchBoxIcon = By.className("search-box-icon");

    public boolean isDisplayedSearchInputField() {
        return isDisplayedElement(searchInputField);
    }

    public WebElement getSearchInputField(){
        return wait.until(ExpectedConditions.visibilityOfElementLocated(searchInputField));
    }

    public void clickSearchBoxIcon(){
        clickElement(searchBoxIcon);
    }


}