package pages;

import configuration.WebHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PrivacyAndCookiesBar extends WebHelper {

    public PrivacyAndCookiesBar(WebDriver driver, WebDriverWait wait){
        super(driver, wait);
    }

    By closePrivacyAndCookiesButton = By.id("cookie-notification-policy-accept-continue");

    public void clickCloseThisMessageAndContinueButton(){
        clickElement(closePrivacyAndCookiesButton);
    }
}