package pages;

import configuration.WebHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.json.JsonOutput;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsPage extends WebHelper {

    public SearchResultsPage(WebDriver driver, WebDriverWait wait){
        super(driver, wait);
    }

    By searchedResult = By.className("productItem__name");
    By nextButton = By.className("arrowRight");
    By secondPage = By.className("isActive");

    public List<WebElement> getResults(){
        wait.until(ExpectedConditions.presenceOfElementLocated(searchedResult));
        List<WebElement> results = driver.findElements(searchedResult);
        return results;
    }

    public void clickNextButton(){
        clickElement(nextButton);
    }

    public WebElement getActivePageNumber(){
        return wait.until(ExpectedConditions.presenceOfElementLocated(secondPage));
    }

    public void clickSearchedElement(int index) {
        List<WebElement> resultsList = getResults();
        resultsList.get(index).click();
    }

    public void waitForSearchedResultToBeInvisible(){
        waitForElementToBeInvisible(searchedResult);
    }
}
