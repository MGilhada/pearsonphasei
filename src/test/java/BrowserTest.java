import configuration.Configuration;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.HomePage;
import pages.PrivacyAndCookiesBar;
import pages.SearchResultsPage;

import java.util.ArrayList;

public class BrowserTest {

    private Configuration config;

    @Before
    public void beforeSteps(){
        Configuration configuration = new Configuration();
        configuration.startBrowser();
        config = configuration;
    }

    @After
    public void afterSteps(){
        config.closeBrowser();
    }

    @Test
    public void displayedSearchInputField() {

        WebDriver driver = config.getDriver();
        WebDriverWait wait = config.getWaitDriver();
        PrivacyAndCookiesBar privacyAndCookiesBar = new PrivacyAndCookiesBar(driver, wait);
        HomePage homePage = new HomePage(driver, wait);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver, wait);

        privacyAndCookiesBar.clickCloseThisMessageAndContinueButton();

        Assert.assertTrue("There is no such element on page", homePage.isDisplayedSearchInputField());
    }

    @Test
    public void maxTenElementsOnTheFirstResultsPage(){
        WebDriver driver = config.getDriver();
        WebDriverWait wait = config.getWaitDriver();
        PrivacyAndCookiesBar privacyAndCookiesBar = new PrivacyAndCookiesBar(driver, wait);
        HomePage homePage = new HomePage(driver, wait);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver, wait);

        privacyAndCookiesBar.clickCloseThisMessageAndContinueButton();

        Assert.assertTrue("There is no such element on page", homePage.isDisplayedSearchInputField());

        homePage.getSearchInputField().sendKeys("Korean");
        homePage.clickSearchBoxIcon();

        Assert.assertTrue("There are more than 10 results", searchResultsPage.getResults().size()<=10);
    }

    @Test
    public void maxTenElementsOnTheNextResultsPage(){
        WebDriver driver = config.getDriver();
        WebDriverWait wait = config.getWaitDriver();
        PrivacyAndCookiesBar privacyAndCookiesBar = new PrivacyAndCookiesBar(driver, wait);
        HomePage homePage = new HomePage(driver, wait);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver, wait);

        privacyAndCookiesBar.clickCloseThisMessageAndContinueButton();

        Assert.assertTrue("There is no such element on page", homePage.isDisplayedSearchInputField());

        homePage.getSearchInputField().sendKeys("Korean");
        homePage.clickSearchBoxIcon();

        Assert.assertTrue("There are more than 10 results", searchResultsPage.getResults().size()<=10);

        searchResultsPage.clickNextButton();

        Assert.assertEquals("It is not second page", Integer.parseInt(searchResultsPage.getActivePageNumber().getText()), 2);
        Assert.assertTrue("There are more than 10 results", searchResultsPage.getResults().size()<=10);
    }

    @Test
    public void navigatetionFromSearchedElementToProperArticle(){
        WebDriver driver = config.getDriver();
        WebDriverWait wait = config.getWaitDriver();
        PrivacyAndCookiesBar privacyAndCookiesBar = new PrivacyAndCookiesBar(driver, wait);
        HomePage homePage = new HomePage(driver, wait);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver, wait);

        privacyAndCookiesBar.clickCloseThisMessageAndContinueButton();

        Assert.assertTrue("There is no such element on page", homePage.isDisplayedSearchInputField());

        homePage.getSearchInputField().sendKeys("Korean");
        homePage.clickSearchBoxIcon();

        Assert.assertTrue("There are more than 10 results", searchResultsPage.getResults().size()<=10);

        searchResultsPage.clickNextButton();

        Assert.assertEquals("It is not second page", Integer.parseInt(searchResultsPage.getActivePageNumber().getText()), 2);
        Assert.assertTrue("There are more than 10 results", searchResultsPage.getResults().size()<=10);

        searchResultsPage.clickSearchedElement(2);
        searchResultsPage.waitForSearchedResultToBeInvisible();
        Assert.assertEquals("Not the correct URL", "https://www.pearson.com/content/dam/one-dot-com/" +
                "one-dot-com/global/Files/news/news-annoucements/2013/2012-RESULTS-PRESENTATION-25-02-2013_WEB.pdf", driver.getCurrentUrl());
    }
}