## PROJECT DESCRIPTION
  
  Pearson Recruitment task.
  
## PROJECT SETUP 
### Project requirements
      1. Installed Java 8+
      2. Installed Gradle

## RUNNING THE TESTS
      1. Before running the tests check, if the version of chromedriver fits the version of the browser you are using.
      2. Depeneding on the platform you are working on please set the value of the property "webdriver.chrome.driver".
         The file can be found in src/main/java/configuration/Configuration.java.      
         Acceptable values are: 
            - "src/main/resources/chromedriver.exe" (Windows)
            - "src/main/resources/chromedriver_linux64" (Linux)
            - "src/main/resources/chromedriver_mac64" (Mac OS)
      3. To run the tests please go to Run->Run... and then select BrowserTest class.